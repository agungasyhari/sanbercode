<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('judul', 45);
            $table->string('isi');
            $table->unsignedBigInteger('jawaban_tepat_id');
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');
        $table->dropForeign('users_id');
        $table->dropColumn('user_id');
        $table->dropForeign('jawaban_tepat_id');
        $table->dropColumn('jawaban_tepat_id');
        $table->dropColumn('isi');
        $table->dropColumn('judul');
        $table->dropColumn('id');

    }
}
